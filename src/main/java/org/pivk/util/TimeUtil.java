package org.pivk.util;

public final class TimeUtil {
    private TimeUtil(){}

    public static long measureTimeNs(Runnable action){
        long startTime = System.nanoTime();

        action.run();

        return System.nanoTime() - startTime;
    }

    public static long measureTimeMs(Runnable action){
        return measureTimeNs(action)/1000000;
    }

    public static long measureTimeS(Runnable action){
        return measureTimeMs(action)/1000;
    }
}
