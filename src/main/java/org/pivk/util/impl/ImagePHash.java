package org.pivk.util.impl;

public class ImagePHash {
    private static ImagePHash ourInstance = new ImagePHash();
    private final ImagePHashImpl phash;

    public static ImagePHash getInstance() {
        return ourInstance;
    }

    private ImagePHash() {
        phash = new ImagePHashImpl();
    }

    public ImagePHashImpl getHash(){
        return phash;
    }
}
