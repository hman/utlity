package org.pivk.util;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public final class DateUtil {
    private DateUtil() {}

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
