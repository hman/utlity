package org.pivk.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@SuppressWarnings("unused")
public final class ZipUtil {
    private ZipUtil(){}

    //TODO use Files instead
    public static String unzip(ZipFile zipFile, String outputFolder) throws IOException {

        boolean firstEntry = true;
        String root = null;

        Enumeration<?> enu = zipFile.entries();
        while (enu.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) enu.nextElement();

            if(firstEntry) {
                root = zipEntry.getName();
                firstEntry = false;
            }

            String name = zipEntry.getName();

            // Do we need to create a directory ?
            File file = new File(outputFolder+name);
            if (name.endsWith(System.getProperty("file.separator"))) {
                file.mkdirs();
                continue;
            }

            File parent = file.getParentFile();
            if (parent != null) {
                parent.mkdirs();
            }

            // Extract the file
            InputStream is = zipFile.getInputStream(zipEntry);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = is.read(bytes)) >= 0) {
                fos.write(bytes, 0, length);
            }
            is.close();
            fos.close();

        }
        zipFile.close();

        return root;
    }
}
