package org.pivk.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.stream.Stream;

@SuppressWarnings({"unused", "WeakerAccess"}) //utility
public final class FileIO {
    private static final Logger log = Logger.getLogger("org.pivk.util.fileio");

    private FileIO(){ throw new UnsupportedOperationException("Utility class instantiation."); }

    /**
     * Reads a file at given location and executes the action on each line.
     *
     * @param inputFile path to the input file
     * @param action to be executed for each line
     * @since 0.1
     */
    public static void forEachLine(String inputFile, Consumer<? super String> action) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(inputFile))) {
            stream.forEach(action);
        }
    }

    /**
     * Read the body of a file as a string.
     *
     * @param inputStream file to be read
     * @return body of file
     * @throws IOException if an I/O error occurs
     * @since 0.1
     */
    public static String readBody(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder body = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            body.append(line).append("\n");
        }

        return body.toString();
    }

    /**
     * Ensures the existence of the directory specified by the parameter.
     * Tries to create the directory with all parents, if needed.
     *
     * @param dir File specifying the intended directory name;
     * @return true if and only if the specified file exists and is a directory
     * @since 0.1
     */
    public static boolean ensureDirectory(File dir) {
        if (dir == null) throw new IllegalArgumentException("Argument 'dir' is null");

        boolean success = true;
        if (!dir.isDirectory()) {
            success = !dir.isFile() && dir.mkdirs();

            if (success) {
                log.info("Created directory: " + dir.toString());
            } else {
                log.severe("Failed while trying to create directory: " + dir.toString());
            }
        }

        return success;
    }

    /**
     * @see #ensureDirectory(String)
     * @since 0.1
     */
    public static boolean ensureDirectory(String dirPath) {
        return ensureDirectory(new File(dirPath));
    }

    /**
     * Tries to overwrite the given file with given content. Ensures that parent directories exist;
     *
     * @param output output file
     * @param content content to be inserted
     * @throws IOException if an I/O error occurs
     * @since 0.1
     */
    public static void overwriteFile(File output, String content) throws IOException {
        if (output == null || content == null) throw new IllegalArgumentException("One argument is null");

        if(!ensureDirectory(output.getParentFile()))
            throw new IOException("Could not create parent directory for file "+ output.toString());

        Files.deleteIfExists(output.toPath());

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(output))) {
            bw.append(content);
            bw.flush();
        }
    }

    public static File isFolder(String path) throws IOException {
        File folder = isFile(new File(path));
        if(!folder.isDirectory()) throw new IOException(folder.getPath()+ " is not a folder");

        return folder;
    }

    public static File isFile(File file) throws IOException {
        if(!file.exists()) throw new FileNotFoundException(file.getPath()+ " does not exist");
        if(!file.canRead()) throw new IOException(file.getPath() + " no read permissions");
        return file;
    }
}