package org.pivk.util;

@SuppressWarnings("unused") //utility
public final class StringUtil {
    private StringUtil(){ throw new UnsupportedOperationException("Utility class instantiation."); }

    /**
     * Lifts the first character of the given string to upper case.
     * <ul>
     *     <li> Returns empty string if input is empty
     * </ul>
     *
     * @param string to be lifted
     * @return string with first letter upper case
     * @throws IllegalArgumentException if input is null
     */
    public static String lift(String string){
        if(string == null) throw new IllegalArgumentException("String input is null");
        if(string.isEmpty()) return string;
        return string.substring(0,1).toUpperCase()+string.substring(1);
    }
}
